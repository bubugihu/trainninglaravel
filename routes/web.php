<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login-admin-backend', function () {
    //
    return "Current url is: Login";
})->name("login");
Route::get('/logout', function () {
     return "<a href=".route("login")." > this is url</a>";
});
Route::get('admin/catalog', function () {
     return "<a href=".route("login")." > this is url</a>";
});
Route::get('admin/product', function () {
     return "<a href=".route("login")." > this is url</a>";
});
Route::group(['prefix'=>'admin'], function (){
    Route::get('/product', function () {
         return "<a href=".route("login")." > this is url</a>";
    });
});
Route::get("/demo","DemoController@getData")->name("demo");
Route::post("/demoPost", "DemoController@postData")->name("postData");
Route::get("/getRecord","DemoController@getRecord")->name("getRecord");
///// bai 2
Route::group(['middleware'=>'checkLogin'], function(){
     Route::get("/listbook", "BookController@index")->name("listbook");
     Route::get("/createbook", "BookController@create")->name("createbook");
     Route::post("/postbook","BookController@postBook")->name("postBook");
     Route::get("/updateBook/{id}", "BookController@update")->name("updateBook");
     Route::post("/postUpdateBook", "BookController@postUpdate")->name("postUpdateBook");
     Route::get("/deleteBook/{id}", "BookController@delete")->name("deleteBook");
     Route::post("/deletePostBook", "BookController@deletePostUpdate")->name("deletePostBook");
     //logout
     Route::get("/logout", "BookController@logout")->name("logout");
});
     
//// bai 3
Route::get("/login", "BookController@login")->name("login");
Route::post("/checkLogin", "BookController@checkLogin")->name("checkLogin");