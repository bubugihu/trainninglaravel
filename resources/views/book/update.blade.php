<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Update Book</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
</head>

<body>
    <div style="margin-left: 20%">
        <form method="POST" action="{{ route("postUpdateBook")}}">
            @csrf
            <h1>Update Book </h1>
            <div>
                <label>Book Id: </label>
                <input type="text" name="id" value="{{ $book->id }}" readonly>
            </div>
            <div>
                <label>Book Title: </label>
                <input type="text" name="title" value="{{ $book->title }}">
            </div>
            <div>
                <label>Book Price: </label>
                <input type="number" name="number"  value="{{ $book->price }}">
            </div>
            <div>
                <input type="submit" name="submit" value="Update" >
            </div>
            <div>
                <a href="{{ route("listbook")}}">
                    <button>Back </button>
                </a>
            </div>
        </form>
    </div>
</body>
</html>
