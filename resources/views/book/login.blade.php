<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
</head>

<body>
    <div style="margin-left: 20%">
       @if(session("flashError"))
        <p style="color:red">{{ session("flashError") }}</p>
       @endif
        <form method="POST" action="{{ route('checkLogin') }}">
            @csrf
            <h1>Login</h1>
            <div>
                <label for="username">Username</label>
                <input type="text" name="username" id="username" placeholder="Enter your username" autocomplete="off">
            </div>
            <div>
                <label for="pass">Password</label>
                <input type="password" name="pass" id="pass" placeholder="Enter your password" autocomplete="off" >
            </div>
            <input type="submit" name="login" value="Submit" >
        </form>
        
        
    </div>
    <script>
        // $(document).ready(function() {
        //     $('#deletebtn').click(function)
        //     {
        //         confirm("Do you want to delete ?")
        //     }
        // }
    </script>
</body>

</html>
