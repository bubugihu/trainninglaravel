<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Create a new Book</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
</head>

<body>
    <div style="margin-left: 20%">
       @if($errors->any())
        <div>
            @foreach($errors->all() as $e)
                <p style="color:red"> {{ $e }} </p>
            @endforeach
        </div>
       @endif

        <form method="POST" id="postBook" action="{{ route('postBook') }}">
            @csrf
            <h1>Create new Book </h1>
            <div>
                <label>Book Title: </label>
                <input type="text" name="title" id="title" value=" {{ old("title") }}">
                @if($errors->first('title'))
                    <label style="color:red"> {{ $errors->first('title') }} </label>  
                @endif
            </div>
            <div>
                <label>Book Price: </label>
                <input type="number" name="price" value=" {{ old("price") }}">
                @if($errors->first('price'))
                    <label style="color:red"> {{ $errors->first('price') }} </label>  
                @endif
            </div>
            <div>
                <input type="button" name="btn" id="Create" value="Create">
            </div>
            <div>
                <a href="{{ route('listbook') }}">
                    <button>Back </button>
                </a>
            </div>
        </form>
    </div>
    <script>
    $(document).ready(function() {
        // $('#Create').click(function (){
        //     var pattern =  /^\w{4,10}$/i;
        //     var name = $('#title').val()
        //     if(!pattern.test(name))
        //     {
        //         alert("error Number")
        //     }
        // });
    })
    </script>
</body>

</html>
