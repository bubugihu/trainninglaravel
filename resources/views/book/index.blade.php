<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>List Book</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
</head>

<body>
    <div style="margin-left: 20%">
        <h1>List Book </h1>
        <a href=" {{ route('createbook') }}">Create new book </a>
        <table border="1">
            <tr>
                <th>Book Id</th>
                <th>Title</th>
                <th>Price</th>
                <th colspan="2">Function</th>
            </tr>
            @foreach ($books as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->price }}</td>
                    <td><a href="{{ url("updateBook/{$book->id}") }}">Update</a></td>
                    {{-- <td><a href="{{ url("deleteBook/{$book->id}") }}">Remove</a></td> --}}
                    <td>
                        <form method="POST" action=" {{ route('deletePostBook') }} ">
                            @csrf
                            <input type="hidden" name="id" value="{{ $book->id }}">
                            <input type="submit" id="deletebtn" name="delete" value="Delete">
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

        <a href="{{ url("/logout") }}">
            <button> Logout </button>
        </a>
    </div>
    <script>
        // $(document).ready(function() {
        //     $('#deletebtn').click(function)
        //     {
        //         confirm("Do you want to delete ?")
        //     }
        // }
    </script>
</body>

</html>
