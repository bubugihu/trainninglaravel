<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Demo Laravel</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        <form method="POST" id="formPost" class="class" action="{{ route('postData') }}">
            @csrf
            <input type="text" name="id" id="id" value="" placeholder="id">
            <input type="text" name="name" class="name" value="" placeholder="name">
            <input type="button" id="formSubmit" value="Submit">
            <!-- Equivalent to... -->
            {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
            {{-- <input type="submit" name="abc" id="abc" value="Submit"> --}}

        </form>
    </div>
    <script>
        $(document).ready(function() {
            $("#formSubmit").click(function() {
                var id = $("#id").val();
                var name = $(".name").val();
                var patternEmail = /^\d+$/i;
                if(!patternEmail.test(id))
                {
                    alert("error");
                }
                // $("#formPost").submit()
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('postData') }}",
                    data: {id:id,name:name},
                    dataType: "json",
                    beforeSend: function() {
                        
                    },
                    success: function(response) {
                        if (response.status) {
                           alert(response.message)
                        }
                    },
                    error: function(error) {
                       
                    },
                    complete: function(error){
                    }
                });
            })

        });
    </script>
</body>

</html>
