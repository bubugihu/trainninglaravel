<?php

use Illuminate\Database\Seeder;

class bookSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $fake = Faker\Factory::create();
        $limit = 10;
        for($i = 0 ; $i < $limit ; $i++)
        {
            DB::table('book')->insert([
                'title' => $fake->name,
                'price' => $i*1000
            ]);
        }
        // DB::table('book')->insert([
        //     'title' => "admin",
        //     'price' => "123",
        // ]);
        
    }
}
