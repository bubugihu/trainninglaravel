<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
   // //khai báo table ứng với model
    protected $table ="book";
    //khai báo trường khóa chính
    protected $primaryKey='id';
    //mặc định khóa chính sẽ tự động tăng
    public $incrementing = true;  //false: khóa chính sẽ tự động tăng 
    protected $fillable = ['id', 'title' ,'price'];
}
