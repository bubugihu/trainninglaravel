<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|min:5",
            "price" => "max:2"
        ];
    }


    /**
     * return messages of validate.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "title.required" => "This is required",
            "title.min" => "Min character is 5",
            "price.max" => "Max character is 2",
        ];
    }
}
