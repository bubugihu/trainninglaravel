<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Demo;
use App\Http\Controllers\Response;
class DemoController extends Controller
{
    function getData()
    {
        return View("demo");
    }
    function postData(Request $request)
    {     
        // echo '<pre>' , var_dump("bingo") , '</pre>';die;
        // $allRecords = Demo::all();
        // foreach($allRecords as $record)
        // {
        //     echo $record->name;
        // }
        try{

            return response()->json([
                'status' => true,
                'message' => "ok"
            ], 200);
        }
        catch(\Throwable $th)
        {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage(),
            ], 500);
        }
    }
    function getRecord()
    {   
        $allRecords = Demo::all();
        foreach($allRecords as $record)
        {
            echo $record->name . " Class: " .$record->class;
        }
    }
}
