<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\MakeRequest;
use App\Book;
use App\Http\Requests\CreateRequest;
use App\User;
class BookController extends Controller
{
    //index
    function index(Request $request)
    {   
        // if(!$request->session()->has('user'))
        // {
        //     return redirect('/login');
        // }
        $books = Book::all();
        return view("book.index", compact('books'));
    }
    //create get
    function create()
    {
        return view("book.create");
    }
    //create post
    function postBook(CreateRequest $request)
    {
        $title = $request["title"];
        $number = $request["price"];
        //insert
        $book = new Book();
        $book->title = $title;
        $book->price = $number;
        $book->save();
        return redirect('/listbook');
    }
    //update get
    function update(Request $request)
    {
        $id = $request["id"];
        $book = Book::find($id);
        return view("book.update",compact("book"));
    }
    //update post
    function postUpdate(Request $request)
    {
        $id = $request["id"];
        $title = $request["title"];
        $price = $request["number"];
        $book = Book::find($id);
        $book->title = $title;
        $book->price = $price;
        $book->save();
        return $this->index();
    }
    //delete get
    function delete(Request $request)
    {
        $id = $request["id"];
        $book = Book::find($id)->delete();
        return $this->index();
    }
    //delete post
    function deletePostUpdate(Request $request)
    {
        $id = $request["id"];
        $book = Book::find($id)->delete();
        return $this->index();
    }
    // login get
    function login()
    {
        return view('book.login');
    }
    //login post
    function checkLogin(Request $request)
    {
        $name = $request['username'];
        $pass = $request['pass'];
        $checkLogin = User::where("name",$name)->where("password",$pass)->first();
        if($checkLogin != null)
        {   
            $request->session()->push('user',$checkLogin);
            return redirect('/listbook');
        }
        else{
            $request->session()->flash("flashError", "Error huh ?");
            return redirect('/login');
        }
    }
    //logout
    function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
