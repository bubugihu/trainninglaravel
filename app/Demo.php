<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{   
    protected $table ="demo"; // teen bang Object Relation Mapping
    protected $fillable = [
        'id', 'age', 'class', 'mark'
    ];
}










// //khai báo table ứng với model
// protected $table ="products";
// //khai báo trường khóa chính
// protected $primaryKey='id';
// //mặc định khóa chính sẽ tự động tăng
// public $incrementing = false;  //false: khóa chính sẽ tự động tăng 
// protected $fillable = ['id', 'name','price','description','image','updated_at','created_at'];
